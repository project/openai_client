<?php

namespace Drupal\openai_client\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Url;
use Drupal\openai_client\OpenAIClientWrapper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Returns responses for OpenAI Client routes.
 */
class AssistantsListController extends ControllerBase {

  /**
   * OpenAI client.
   *
   * @var \Drupal\openai_client\OpenAIClientWrapper
   */
  protected OpenAIClientWrapper $api;

  /**
   * Data formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected DateFormatter $dateFormatter;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a new AssistantsListController object.
   *
   * @param \Drupal\openai_client\OpenAIClientWrapper $api
   *   Open API wrapper.
   * @param \Drupal\Core\Datetime\DateFormatter $dateFormatter
   *   Date formatter.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   */
  public function __construct(OpenAIClientWrapper $api, DateFormatter $dateFormatter, RequestStack $requestStack) {
    $this->api = $api;
    $this->dateFormatter = $dateFormatter;
    $this->requestStack = $requestStack;

    // Clear add/edit assistant form.
    $session = $this->getRequest()->getSession();
    $session->remove('openai_client_assistant_edit');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('openai_client.wrapper'),
      $container->get('date.formatter'),
      $container->get('request_stack')
    );
  }

  /**
   * Builds the response.
   */
  public function build() {
    try {
      $assistants = $this->api->getAssistants();
    }
    catch (\Exception $e) {
      $this->messenger()->addError($e->getMessage());

      return [
        '#type' => 'item',
        '#markup' => $this->t('Connection error'),
      ];
    }

    $build['add_new'] = [
      '#type' => 'link',
      '#title' => $this->t('Add new assistant'),
      '#attributes' => [
        'class' => ['button', 'button--primary'],
      ],
      '#url' => Url::fromRoute('openai_client.assistants_add'),
    ];

    $rows = [];
    foreach ($assistants->data as $result) {
      $date = date($result->createdAt);
      $rows[] = [
        $result->name,
        [
          'data' => [
            [
              '#type' => 'link',
              '#title' => $result->id,
              '#url' => Url::fromRoute(
                'openai_client.assistants_edit',
                [
                  'assistant_id' => $result->id,
                ]
              ),
            ],
            [
              '#type' => 'item',
              '#markup' => $result->description,
            ],
          ],
        ],
        $result->object,
        $this->dateFormatter->format($date, 'custom', 'Y-m-d H:i'),
        $result->model,
        [
          'data' => [
            '#type' => 'details',
            '#title' => $this->t('Details'),
            '#open' => FALSE,
            [
              '#type' => 'item',
              '#title' => $this->t('Instructions'),
              '#markup' => $result->instructions,
            ],
            [
              '#type' => 'item',
              '#title' => $this->t('Tools'),
              '#markup' => Json::encode($result->tools),
            ],
          ],
        ],
      ];
    }

    $build['list'] = [
      '#type' => 'table',
      '#caption' => $this->t('Assistants'),
      '#header' => [
        $this->t('Name'),
        $this->t('Id'),
        $this->t('Object'),
        $this->t('Created at'),
        $this->t('Model'),
        $this->t('Meta'),
      ],
      '#rows' => $rows,
    ];

    return $build;
  }

  /**
   * Gets the request object.
   *
   * @return \Symfony\Component\HttpFoundation\Request
   *   The request object.
   */
  protected function getRequest() {
    return $this->requestStack->getCurrentRequest();
  }

}
