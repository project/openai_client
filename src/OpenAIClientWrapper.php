<?php

namespace Drupal\openai_client;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use OpenAI\Client;
use OpenAI\Responses\Assistants\AssistantListResponse;

/**
 * Service description.
 */
class OpenAIClientWrapper {

  public const IMAGE_SIZE_256 = '256x256';
  public const IMAGE_SIZE_512 = '512x512';
  public const IMAGE_SIZE_1024 = '1024x1024';

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The uuid service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuid;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs an OpenAIClientWrapper object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   *   The uuid service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(ConfigFactoryInterface $config_factory, UuidInterface $uuid, MessengerInterface $messenger) {
    $this->configFactory = $config_factory;
    $this->uuid = $uuid;
    $this->messenger = $messenger;
  }

  /**
   * Get a new client with token set.
   *
   * @param string $token
   *   Optional API token, by default use the module set token.
   *
   * @return \OpenAI\Client
   *   Started AI client with token if defined in parameters or settings.
   */
  public function getClient(string $token = ''): Client {
    // If token variable is empty then $this->getApiToken()
    // will return value, in case it also return null then
    // $token will set a empty string.
    $token = $token ?: ($this->getApiToken() ?? '');
    return \OpenAI::client($token);
  }

  /**
   * Get available AI models.
   *
   * @return array
   *   Model list.
   */
  public function getModels(): array {
    $response = $this->getClient()->models()->list();
    return $response->toArray()['data'];
  }

  /**
   * Get created assistants.
   *
   * @return \OpenAI\Responses\Assistants\AssistantListResponse
   *   Assistant list.
   */
  public function getAssistants(): AssistantListResponse {
    return $this->getClient()->assistants()->list();
  }

  /**
   * Has module API token set?
   *
   * @return bool
   *   TRUE if we have defined token.
   */
  public function hasToken():bool {
    return !empty($this->getApiToken());
  }

  /**
   * Do a direct query to default chat model and return the response text.
   *
   * @param string $prompt
   *   User message.
   *
   * @return string
   *   Response string.
   */
  public function fastChatQuery(string $prompt): string {
    try {
      $message = [
        'model' => $this->getDefaultModel(),
        'messages' => [['role' => 'user', 'content' => $prompt]],
      ];

      $response = $this->getClient()->chat()->create($message);

      return $response->choices[0]->message->content;
    }
    catch (\Exception $e) {
      $this->messenger->addError($e->getMessage());
      return $prompt;
    }
  }

  /**
   * Get default module chat model.
   *
   * @return array|mixed|null
   *   Default model ID.
   */
  public function getDefaultModel() {
    $config = $this->configFactory->get('openai_client.settings');
    return $config->get('api-chat-model');
  }

  /**
   * Generate a new image from prompt.
   *
   * @param string $prompt
   *   Text prompt.
   * @param int $amount
   *   Images size, the service class define constants to it. Smaller sizes are
   *   faster to generate.
   * @param string $size
   *   You can request 1-10 images at a time.
   *
   * @return array|\array{created: int, data: array}
   *   Image creation result.
   */
  public function imageCreate(string $prompt, int $amount = 1, string $size = self::IMAGE_SIZE_256) {
    if ($amount < 1 || $amount > 10) {
      throw new \InvalidArgumentException(sprintf('$amount must be number between 1 & 10, "%s" provided', $amount));
    }

    $response = $this->getClient()->images()->create([
      'prompt' => $prompt,
      'n' => $amount,
      'size' => $size,
      'response_format' => 'url',
    ]);

    return $response->toArray();
  }

  /**
   * Get valid image sizes to create.
   *
   * @return string[]
   *   Allowed image sizes.
   */
  public static function getImageSizes() {
    return [
      '256x256' => self::IMAGE_SIZE_256,
      '512x512' => self::IMAGE_SIZE_512,
      '1024x1024' => self::IMAGE_SIZE_1024,
    ];
  }

  /**
   * Get chat system prompt.
   *
   * @return array|mixed|null
   *   System prompt.
   */
  public function getChatSystemPrompt() {
    $config = $this->configFactory->get('openai_client.settings');
    return $config->get('system_prompt');
  }

  /**
   * Get OpenAI API token.
   *
   * @return array|mixed|null
   *   API token.
   */
  protected function getApiToken() {
    $config = $this->configFactory->get('openai_client.settings');
    return $config->get('api-token');
  }

}
