<?php

namespace Drupal\openai_client\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\openai_client\OpenAIClientWrapper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure OpenAI Client settings for this site.
 *
 * @see chat_workflow https://platform.openai.com/docs/guides/chat
 */
class SettingsForm extends ConfigFormBase {

  /**
   * OpenAi client.
   *
   * @var \Drupal\openai_client\OpenAIClientWrapper
   */
  private OpenAIClientWrapper $api;

  /**
   * Date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  private DateFormatter $dateFormatter;

  /**
   * Constructs a new SettingsForm object.
   *
   * @param \Drupal\openai_client\OpenAIClientWrapper $api
   *   Open API wrapper.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Datetime\DateFormatter $dateFormatter
   *   Date formatter.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager
   *   The typed config manager.
   */
  public function __construct(OpenAIClientWrapper $api, ConfigFactoryInterface $config_factory, DateFormatter $dateFormatter, TypedConfigManagerInterface $typed_config_manager) {
    parent::__construct($config_factory, $typed_config_manager);

    $this->api = $api;
    $this->dateFormatter = $dateFormatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('openai_client.wrapper'),
      $container->get('config.factory'),
      $container->get('date.formatter'),
      $container->get('config.typed'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'openai_client_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['openai_client.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Help links.
    $links = [];
    $links['openai-documentation'] = [
      '#type' => 'link',
      '#title' => self::t('OpenAI API documentation'),
      '#url' => Url::fromUri('https://platform.openai.com/docs/introduction/overview'),
      '#attributes' => [
        'target' => '_blank',
        'rel' => 'noopener noreferrer',
      ],
    ];

    $links['openai-api-reference'] = [
      '#type' => 'link',
      '#title' => self::t('OpenAI API reference'),
      '#url' => Url::fromUri('https://platform.openai.com/docs/api-reference'),
      '#attributes' => [
        'target' => '_blank',
        'rel' => 'noopener noreferrer',
      ],
    ];

    $links['openai-php-github'] = [
      '#type' => 'link',
      '#title' => self::t('OpenAI PHP in Github'),
      '#url' => Url::fromUri('https://github.com/openai-php/client'),
      '#attributes' => [
        'target' => '_blank',
        'rel' => 'noopener noreferrer',
      ],
    ];

    $form['doc-links'] = [
      '#theme' => 'item_list',
      '#items' => $links,
      '#title' => $this->t('Documentation links'),
    ];

    // Settings form.
    $apiToken = $this->config('openai_client.settings')->get('api-token');
    $form['api-token'] = [
      '#type' => 'password',
      '#title' => $this->t('OpenAI API Token'),
      '#default_value' => $apiToken,
      '#description' => (!empty($apiToken) ? $this->t('The token is set.') : ''),
    ];

    $form['system_prompt'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Generic conversation system prompt'),
      '#default_value' => $this->config('openai_client.settings')->get('system_prompt'),
      '#description' => $this->t('Initial system prompt to provide context about personality and set the tone for AI interactions.'),
    ];

    if ($this->api->hasToken()) {
      $form['api-chat-model'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Chat model'),
        '#default_value' => $this->config('openai_client.settings')->get('api-chat-model'),
      ];

      try {
        $models = $this->api->getModels();
      }
      catch (\Exception $e) {
        $models = [];
      }
      if (count($models) > 0) {
        $modelGroups = [];
        foreach ($models as $i => $model) {
          if (!isset($modelGroups[$models[$i]['owned_by']])) {
            $modelGroups[$models[$i]['owned_by']] = [];
          }
          $modelGroups[$models[$i]['owned_by']][] = $model;
        }
        $form['grp_models'] = [
          '#type' => 'details',
          '#title' => $this->t('Models'),
          '#open' => FALSE,
        ];
        foreach ($modelGroups as $owner => &$modelGroup) {
          usort($modelGroup, function ($a, $b) {
            if ($a['created'] == $b['created']) {
              return 0;
            }
            return ($a['created'] < $b['created']) ? 1 : -1;
          });
          $form['grp_models']['grp_' . $owner] = [
            '#type' => 'details',
            '#title' => $this->t('@owner models', ['@owner' => $owner]),
            '#open' => FALSE,
          ];
          $form['grp_models']['grp_' . $owner]['api-models-' . $owner] = [
            '#type' => 'table',
            '#header' => [
              '#',
              $this->t('ID'),
              $this->t('Object'),
              $this->t('Created'),
              $this->t('Owner'),
            ],
          ];
          foreach ($modelGroup as $i => $model) {
            $form['grp_models']['grp_' . $owner]['api-models-' . $owner][$i]['index'] = [
              '#type' => 'item',
              '#markup' => $i,
            ];
            $form['grp_models']['grp_' . $owner]['api-models-' . $owner][$i]['id'] = [
              '#type' => 'item',
              '#markup' => $model['id'] ?? '',
            ];
            $form['grp_models']['grp_' . $owner]['api-models-' . $owner][$i]['object'] = [
              '#type' => 'item',
              '#markup' => $model['object'] ?? '',
            ];
            $date = date($model['created']);
            $form['grp_models']['grp_' . $owner]['api-models-' . $owner][$i]['created'] = [
              '#type' => 'item',
              '#markup' => $this->dateFormatter->format($date, 'Y-m-d'),
            ];
            $form['grp_models']['grp_' . $owner]['api-models-' . $owner][$i]['owned_by'] = [
              '#type' => 'item',
              '#markup' => $model['owned_by'] ?? '',
            ];
          }
        }
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $token = $form_state->getValue('api-token');
    if (empty($token)) {
      $token = $this->config('openai_client.settings')->get('api-token');
    }
    if (!empty($token)) {
      try {
        $this->api->getClient($token)->models()->list();
      }
      catch (\Exception $e) {
        $form_state->setErrorByName('api-token', $e->getMessage());
      }
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('openai_client.settings');
    if (!empty($form_state->getValue('api-token'))) {
      $config->set('api-token', trim($form_state->getValue('api-token')));
    }
    $model = $form_state->getValue('api-chat-model');
    if (empty($model)) {
      $model = '';
    }
    $config->set('api-chat-model', trim($model));
    $config->set('system_prompt', $form_state->getValue('system_prompt'));
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
