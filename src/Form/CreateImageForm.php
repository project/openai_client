<?php

namespace Drupal\openai_client\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\openai_client\OpenAIClientWrapper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a OpenAI Client form.
 */
class CreateImageForm extends FormBase {

  /**
   * OpenAI client.
   *
   * @var \Drupal\openai_client\OpenAIClientWrapper
   */
  protected OpenAIClientWrapper $api;

  /**
   * Constructs a new CreateImageForm object.
   *
   * @param \Drupal\openai_client\OpenAIClientWrapper $api
   *   OpenAI client API.
   */
  public function __construct(OpenAIClientWrapper $api) {
    $this->api = $api;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('openai_client.wrapper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'openai_client_create_image';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $rebuildInfo = $form_state->getRebuildInfo();
    if (isset($rebuildInfo['images'])) {
      foreach ($rebuildInfo['images'] as $key => $image) {
        $form[$key] = [
          '#type' => 'markup',
          '#markup' => '<img src="' . $image . '"/>',
        ];
      }
    }

    $form['prompt'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Create image prompt'),
      '#required' => TRUE,
    ];

    $form['amount'] = [
      '#type' => 'number',
      '#title' => $this->t('Amount'),
      '#default_value' => $rebuildInfo['amount'] ?? 1,
      '#required' => TRUE,
      '#min' => 1,
      '#max' => 10,
    ];

    $form['size'] = [
      '#type' => 'select',
      '#title' => $this->t('Image size'),
      '#required' => TRUE,
      '#default_value' => $rebuildInfo['size'] ?? OpenAIClientWrapper::IMAGE_SIZE_256,
      '#options' => OpenAIClientWrapper::getImageSizes(),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $prompt = $form_state->getValue('prompt');
    $amount = $form_state->getValue('amount');
    $size = $form_state->getValue('size');

    $images = $this->api->imageCreate($prompt, $amount, $size);
    if (isset($images["data"])) {
      $imageUrls = [];
      foreach ($images["data"] as $image) {
        $imageUrls[] = $image['url'];
      }
      $form_state->setRebuildInfo(['images' => $imageUrls]);
      $this->messenger()->addStatus($this->t('The image/s was generated.'));
    }
    else {
      $this->messenger()->addWarning($this->t('The image/s was not generated.'));
    }

    $form_state->setRebuild();
  }

}
