<?php

namespace Drupal\openai_client\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\openai_client\OpenAIClientWrapper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a OpenAI Client form.
 */
class AssistantEditForm extends FormBase {

  /**
   * OpenAI client.
   *
   * @var \Drupal\openai_client\OpenAIClientWrapper
   */
  protected OpenAIClientWrapper $api;

  /**
   * Constructs a new OpenAIClientWrapper object.
   *
   * @param \Drupal\openai_client\OpenAIClientWrapper $api
   *   Open API wrapper.
   */
  public function __construct(OpenAIClientWrapper $api) {
    $this->api = $api;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('openai_client.wrapper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'openai_client_assistant_edit';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $formData = [
      'name' => '',
      'instructions' => '',
      'tools' => '',
      'description' => '',
      'metadata' => '',
      'model' => '',
    ];
    $session = $this->getRequest()->getSession();
    $values = $session->get($this->getFormId());
    $request = $this->getRequest();
    $assistantId = $request->get('assistant_id');
    if ($values) {
      $formData = $values;
    }
    elseif ($assistantId) {
      $response = NULL;
      try {
        $response = $this->api->getClient()->assistants()->retrieve($assistantId);
      }
      catch (\Exception $e) {
        $this->messenger()->addError($e->getMessage());
      }
      if ($response) {
        $formData = [
          'name' => $response->name,
          'instructions' => $response->instructions,
          'tools' => Json::encode($response->tools),
          'description' => $response->description,
          'metadata' => Json::encode($response->meta()->toArray()),
          'model' => $response->model,
        ];
      }
    }
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#required' => TRUE,
      '#default_value' => $formData['name'],
    ];
    $form['model'] = [
      '#type' => 'select',
      '#title' => $this->t('Model'),
      '#required' => TRUE,
      '#description' => $this->t(
        'ID of the model to use. You can use the @list API to see all of your available models, or see our @models for descriptions of them.',
        [
          '@list' => 'List models',
          '@models' => 'Model overview',
        ]
      ),
      '#options' => $this->getModelOptions(),
      '#value' => $formData['model'],
    ];
    $form['instructions'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Instructions'),
      '#default_value' => $formData['instructions'],
      '#description' => $this->t('The system instructions that the assistant uses. The maximum length is 32768 characters.'),
    ];
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#description' => $this->t('The description of the assistant. The maximum length is 512 characters.'),
      '#default_value' => $formData['description'],
    ];
    $form['tools'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Tools'),
      '#default_value' => $formData['tools'],
      '#description' => $this->t('A list of tool enabled on the assistant. There can be a maximum of 128 tools per assistant. Tools can be of types code_interpreter, retrieval, or function.'),
    ];
    $form['metadata'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Metadata'),
      '#default_value' => $formData['metadata'],
      '#description' => $this->t('Set of 16 key-value pairs that can be attached to an object. This can be useful for storing additional information about the object in a structured format. Keys can be a maximum of 64 characters long and values can be a maximum of 512 characters long.'),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Persist form user data.
    $values = $form_state->getValues();
    $session = $this->getRequest()->getSession();
    $session->set($this->getFormId(), $values);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    try {
      $input = $form_state->getUserInput();
      $data = [
        "name" => $values['name'],
        "model" => $input['model'],
      ];
      if (!empty($values['instructions'])) {
        $data['instructions'] = $values['instructions'];
      }
      if (!empty($values['tools'])) {
        $data['tools'] = $values['tools'];
      }
      if (!empty($values['description'])) {
        $data['description'] = $values['description'];
      }
      if (!empty($values['metadata'])) {
        $data['metadata'] = $values['metadata'];
      }
      $request = $this->getRequest();
      $assistantId = $request->get('assistant_id');
      if ($assistantId) {
        $this->api->getClient()->assistants()->modify($assistantId, $data);
      }
      else {
        $this->api->getClient()->assistants()->create($data);
      }
      $session = $this->getRequest()->getSession();
      $session->remove($this->getFormId());
      $this->messenger()->addStatus($this->t('Assistant saved'));
    }
    catch (\Exception $e) {
      $this->messenger()->addError($e->getMessage());
      return;
    }
    $form_state->setRedirect('openai_client.assistants_collection');
  }

  /**
   * Get model list how list to use in select control.
   */
  protected function getModelOptions(): array {
    try {
      $models = $this->api->getModels();
    }
    catch (\Exception $e) {
      $models = [];
    }

    $resp = [
      '' => $this->t('- Select -'),
    ];
    if (count($models) > 0) {
      foreach ($models as $model) {
        $resp[$model['id']] = $model['id'];
      }
    }

    ksort($resp);
    return $resp;
  }

}
