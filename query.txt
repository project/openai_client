// phpcs:ignoreFile
Please, generate to me a README.md file for a new Drupal 10 module. In the install section, please, use composer. The module code is:

The file names are indicated as follows: ==> Name and path to the file <==

==> ./openai_client.services.yml <==
services:
  openai_client.wrapper:
    class: Drupal\openai_client\OpenAIClientWrapper
    arguments: ['@config.factory', '@uuid', '@messenger']

==> ./openai_client.routing.yml <==
openai_client.settings_form:
  path: '/admin/config/system/openai-client'
  defaults:
    _title: 'OpenAI Client settings'
    _form: 'Drupal\openai_client\Form\SettingsForm'
  requirements:
    _permission: 'administer openai_client configuration'

==> ./openai_client.links.menu.yml <==
# Settings link.
openai_client.settings_form:
  title: 'OpenAI Client'
  description: 'Configure OpenAI Client.'
  parent: system.admin_config_system
  route_name: openai_client.settings_form
  weight: 10

==> ./config/schema/openai_client.schema.yml <==
# Schema for the configuration files of the OpenAI Client module.
openai_client.settings:
  type: config_object
  label: 'OpenAI Client settings'
  mapping:
    example:
      type: string
      label: 'Example'

==> ./src/Form/SettingsForm.php <==
<?php

namespace Drupal\openai_client\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\openai_client\OpenAIClientWrapper;
use OpenAI\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure OpenAI Client settings for this site.
 *
 * @see chat_workflow https://platform.openai.com/docs/guides/chat
 */
class SettingsForm extends ConfigFormBase {

  /**
   * @var \Drupal\openai_client\OpenAIClientWrapper
   */
  private OpenAIClientWrapper $api;

  /**
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  private DateFormatter $dateFormatter;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(OpenAIClientWrapper $api, ConfigFactoryInterface $config_factory, DateFormatter $dateFormatter) {
    parent::__construct($config_factory);

    $this->api = $api;
    $this->dateFormatter = $dateFormatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('openai_client.wrapper'),
      $container->get('config.factory'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'openai_client_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['openai_client.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $apiToken = $this->config('openai_client.settings')->get('api-token');
    $form['api-token'] = [
      '#type' => 'password',
      '#title' => $this->t('OpenAI API Token'),
      '#default_value' => $apiToken,
      '#description' => (!empty($apiToken) ? $this->t('The token is set.') : ''),
    ];

    if ($this->api->hasToken()) {
      try {
        $models = $this->api->getModels();
      }
      catch (\Exception $e) {
        $models = [];
      }
      if (count($models) > 0) {
        $modelGroups = [];
        foreach ($models as $i => $model) {
          if (!isset($modelGroups[$models[$i]['owned_by']])) {
            $modelGroups[$models[$i]['owned_by']] = [];
          }
          $modelGroups[$models[$i]['owned_by']][] = $model;
        }
        foreach ($modelGroups as $owner => &$modelGroup) {
          usort($modelGroup, function ($a, $b) {
            if ($a['created'] == $b['created']) {
              return 0;
            }
            return ($a['created'] < $b['created']) ? 1 : -1;
          });
          $form['api-models-' . $owner] = [
            '#type' => 'table',
            '#caption' => $this->t('@owner models', ['@owner' => $owner]),
            '#header' => [
              '#',
              $this->t('ID'),
              $this->t('Object'),
              $this->t('Created'),
              $this->t('Owner'),
              $this->t('root'),
            ],
          ];
          foreach ($modelGroup as $i => $model) {
            $form['api-models-' . $owner][$i]['index'] = ['#type' => 'item', '#markup' => $i];
            $form['api-models-' . $owner][$i]['id'] = ['#type' => 'item', '#markup' => $model['id']];
            $form['api-models-' . $owner][$i]['object'] = ['#type' => 'item', '#markup' => $model['object']];
            $date = date($model['created']);
            $form['api-models-' . $owner][$i]['created'] = ['#type' => 'item', '#markup' => $this->dateFormatter->format($date, 'Y-m-d')];
            $form['api-models-' . $owner][$i]['owned_by'] = ['#type' => 'item', '#markup' => $model['owned_by']];
            $form['api-models-' . $owner][$i]['root'] = ['#type' => 'item', '#markup' => $model['root']];
          }
        }

        $form['api-chat-model'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Chat model'),
          '#default_value' => $this->config('openai_client.settings')->get('api-chat-model'),
        ];
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $token = $form_state->getValue('api-token');
    if (empty($token)) {
      $token = $this->config('openai_client.settings')->get('api-token');
    }
    if (!empty($token)) {
      try {
        $this->api->getClient($token)->models()->list();
      }
      catch (\Exception $e) {
        $form_state->setErrorByName('api-token', $e->getMessage());
      }
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('openai_client.settings');
    if (!empty($form_state->getValue('api-token'))) {
      $config->set('api-token', trim($form_state->getValue('api-token')));
    }
    $model = $form_state->getValue('api-chat-model');
    if (empty($model)) {
      $model = '';
    }
    $config->set('api-chat-model', trim($model));
    $config->save();
    parent::submitForm($form, $form_state);
  }

}

== > . / src / OpenAIClientWrapper . php <= =
< ? php

namespace Drupal\openai_client;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;

Client;

/**
 * Service description.
 */
class OpenAIClientWrapper {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The uuid service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuid;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs an OpenAIClientWrapper object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   *   The uuid service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(ConfigFactoryInterface $config_factory, UuidInterface $uuid, MessengerInterface $messenger) {
    $this->configFactory = $config_factory;
    $this->uuid = $uuid;
    $this->messenger = $messenger;
  }

  /**
   * Get a new client with token set.
   *
   * @return \OpenAI\Client
   */
  public function getClient(string $token = ''): Client {
    if (empty($token)) {
      $token = $this->getApiToken();
    }
    return \OpenAI::client($token);
  }

  /**
   * Get available AI models.
   *
   * @return array
   */
  public function getModels(): array {
    $response = $this->getClient()->models()->list();
    return $response->toArray()['data'];
  }

  /**
   * Has module API token set?
   *
   * @return bool
   */
  public function hasToken():bool {
    return !empty($this->getApiToken());
  }

  /**
   *
   */
  public function fastChatQuery(string $prompt): string {
    $message = [
      'model' => $this->getDefaultModel(),
      'messages' => [['role' => 'user', 'content' => $prompt]],
    ];

    $response = $this->getClient()->chat()->create($message);

    return $response->choices[0]->message->content;
  }

  /**
   * Get default module chat model.
   *
   * @return array|mixed|null
   */
  public function getDefaultModel() {
    $config = $this->configFactory->get('openai_client.settings');
    return $config->get('api-chat-model');
  }

  /**
   * Get OpenAI API token.
   *
   * @return array|mixed|null
   */
  protected function getApiToken() {
    $config = $this->configFactory->get('openai_client.settings');
    return $config->get('api-token');
  }

}

== > . / openai_client . permissions . yml <= =
administer openai_client configuration:
  title: 'Administer openai_client configuration'
  description: ''
  restrict access: TRUE

== > . / composer . json <= =
{
"name" : "drupal/openai_client",
    "type": "drupal-module",
    "description": "OpenAI API integration and basic user interfaces.",
    "keywords": [],
    "homepage": "https://www.drupal.org/project/openai_client",
    "minimum-stability": "dev",
    "support": {
"issues": "https://www.drupal.org/project/issues/openai_client",
        "source": "http://cgit.drupalcode.org/openai_client"
    },
    "require": {
"league/commonmark": "^2.4"
    }
}

== > . / openai_client . info . yml <= =
name: \OpenAI Client
type: module
description: \OpenAI API integration and basic user interfaces .
package: Custom
core_version_requirement: ^ 9 || ^ 10
